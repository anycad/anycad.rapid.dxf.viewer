﻿using System;
using System.Windows;

namespace AnyDraw
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AnyCAD.Foundation.Application.Startup();
            var email = "aaa";
            var uuid = "aaa";
            var sn = "e2b9043c7719a483fbe92fa9b51390aa";
            AnyCAD.Foundation.Application.RegisterSDK(email, uuid, sn);

            AnyCAD.Foundation.DocumentManager.Instance().SetDocType("Drawing");

            AnyCAD.UX.Icons.SetTheme(AnyCAD.UX.EnumTheme.Dark);
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            AnyCAD.Foundation.Application.Exit();
        }
    }
}
